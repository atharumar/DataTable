$(document).ready(function() {

//1. loading Data Table for first time
$('#example').DataTable();  

  
//2. Filtering table contents on drop down  
$('#selectoption').on('change', function() {
  console.log('started')
  var table = $('#example').DataTable();
  var selectObject = this;
  var selectValue = selectObject.value;
  $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex ) {
       selectValue = selectObject.value;
       console.log(selectValue)
       var pos = data[1]
       posList = getPositionList(selectValue);       
       if(posList!=null){
       for( i = 0; i<posList.length; i++ ){
         if( pos == posList[i] ) {
           return true;
         }
       }
       return false;         
       }
      return true;
    }
  );
  table.draw();  
});
//2. ended
  
});



function drawOnSelect(select){
  var table = $('#example').DataTable();
  var selectValue = select.value;
  $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex ) {
       selectValue = select.value;
       var pos = data[1]
       posList = getPositionList(selectValue);       
       if(posList!=null){
       for( i = 0; i<posList.length; i++ ){
         if( pos == posList[i] ) {
           return true;
         }
       }
       return false;         
       }
      return true;
    }
  );
  table.draw();  
}

function getPositionList(select){
  if(select == '1'){
    return ['Chief Executive Officer (CEO)', 'Accountant']
  }
  if(select == '2'){
    return ['Developer','Office Manager'];    
  }
}